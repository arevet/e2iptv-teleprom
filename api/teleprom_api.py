# -*- coding: utf-8 -*-
#  Dreambox Enigma2 iptv player
#
#  Copyright (c) 2013 Alex Revetchi <revetski@gmail.com>
#  Copyright (c) 2010 Alex Maystrenko <alexeytech@gmail.com>
#  web: http://techhost.dlinkddns.com/
#
# This is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2, or (at your option) any later
# version.

import urllib
from abstract_api import MODE_STREAM, AbstractAPI, AbstractStream
from datetime import datetime
from time import time
from md5 import md5
import os
from . import tdSec, secTd, setSyncTime, syncTime, Timezone, APIException
	
class TelepromAPI(AbstractAPI):
	
	iProvider = "teleprom"
	NUMBER_PASS = True
	
	site = "http://a01.teleprom.tv/api/json2" 

	def __init__(self, username, password):
		AbstractAPI.__init__(self, username, password)
		
		self.time_shift = 0
		self.time_zone = 0
		self.settings = {}
		
	def start(self):
		self.authorize()

	def on_accountData(self, account):
		if 'packet_expire' in account:
			self.packet_expire = datetime.fromtimestamp(int(account['packet_expire']))
		if 'pcode' in account:
			self.protect_code = account['pcode']

	def authorize(self):
		self.trace("Username is "+self.username)
		params = {'login' : self.username, 'pass' : self.password, 'hwid': self.uuid, 'device':'enigma2', 'plugin':'2.2', 'tz':Timezone, 'settings':'all'}
		response = self.getJsonData(self.site+"/login?", params, "authorize", 1)
		
		if 'sid' in response:
			self.sid = response['sid'].encode("utf-8")
	
		if 'settings' in response:
			try:
				self.parseSettings(response['settings'])
			except:
				self.settings={}
			if 'timeshift' in response['settings']:
				self.time_shift = int(response['settings']['timeshift']['value'])
			if 'timezone' in response['settings']:
				self.time_zone = int(response['settings']['timezone']['value'])
				
		if 'account' in response:
			self.on_accountData(response['account'])


	def getAccountData(self):
		adata = self.getJsonData(self.site+"/account?", {}, "account data")
		if 'account' in adata:
			self.on_accountData(adata['account']) 
				
	def getChannelsData(self):
		return self.getJsonData(self.site+"/channel_list?", {}, "channels list")

	def getUrlData(self, cid, pin, time):
		params = {'cid': cid}
		if self.channels[cid].is_protected and pin:
			params["protect_code"] = pin
		if time:
			params["gmt"] = time.strftime("%s")
		return self.getJsonData(self.site+"/get_url?", params, "stream url")

	
class e2iptv(TelepromAPI, AbstractStream):
	
	iName = "TelepromTV"
	MODE = MODE_STREAM
	HAS_PIN = True
	
	def __init__(self, username, password):
		TelepromAPI.__init__(self, username, password)
		AbstractStream.__init__(self)
		self.ssclient = os.path.dirname( __file__ ) + '/ssclient'
		self.hlsgw = os.path.dirname( __file__ ) + '/hlsgwpy'
		os.system('start-stop-daemon -K -x python -- '+self.hlsgw)
		os.system('start-stop-daemon -b -S -x python -- ' + self.hlsgw)

	def __del__(self):
		os.system('start-stop-daemon -K -x python -- '+self.hlsgw)

	def epg_entry(self, e, ts_fix):
		txt = ""
		try:
			txt   = e['progname'].encode('utf-8')
		except: pass
		try:
			if len(txt): txt += '\n'
			txt += e['description'].encode('utf-8')
		except: pass

		start = datetime.fromtimestamp(int(e['ut_start'])+ts_fix)
		end   = None
		return ({"text":txt,"start":start,"end":end})

	def channel_day_epg(self, channel):
		if 'epg' in channel:
			for e in channel['epg']:
				yield self.epg_entry(e, self.time_shift)

	def on_channelEpgCurrent(self, channel):
		if 'epg_progname' in channel:
			start = datetime.fromtimestamp(int(channel['epg_start'])+self.time_shift)
			end = datetime.fromtimestamp(int(channel['epg_end'])+self.time_shift)
			yield ({"text":channel['epg_progname'].encode('utf-8'),"start":start,"end":end})
			
	def on_setChannelsList(self):
		channelsData = self.getChannelsData()
		if 'servertime' in channelsData:
			try:
				setSyncTime(datetime.fromtimestamp(channelsData["servertime"]))
			except:
				pass
		
		for group in channelsData['groups']:
			group_id = group['id']
			group_name = group['name'].encode('utf-8')
			if isinstance(group['channels'], list):
				chlist = group['channels']
			else:
				chlist = [group['channels']]
			for channel in chlist: 
				id          = channel['id']
				name        = channel['name'].encode('utf-8') 
				has_archive = ('have_archive' in channel) and (bool(channel['have_archive']))
				is_protected = ('protected' in channel) and channel['protected']
				yield ({"id":id,
					"group_id":group_id,
					"group_name":group_name,
					"name":name,
					"number":id,
					"has_archive":has_archive,
					"is_protected":is_protected,
					"epg_data_opaque":channel})

	def on_getStreamUrl(self, cid, pin, time = None):
		response = self.getUrlData(cid, pin, time)
		if 'hls' in response:
			return "http://127.0.0.1:7001/?" + urllib.quote("url="+response['url'],'')
	
		if not 'sstp' in response:
			raise APIException("Response does not conatin streamming url, or stream protocol not supported.")	
		sstp = response['sstp']
		os.system('start-stop-daemon -K -x '+self.ssclient)
		os.system('start-stop-daemon -b -S -x '+self.ssclient + ' --  -P 5000 -i ' + sstp['ip']+' -p '+str(sstp['port'])+' -u '+sstp['login']+' -k '+sstp['key']+' -d 2 -b 64')
		return "http://127.0.0.1:5000"
		
	def on_getChannelsEpg(self, cids):
		params = {"epg":3, "cids" : ",".join(map(str, cids))}
		response = self.getJsonData(self.site+"/epg_current?", params, "EPG for channels")
		for cnl in response['epg']:
			id = cnl['cid']
			if 'epg' in cnl:
				for e in cnl['epg']:
					txt = e['progname'].encode('utf-8')
					start = datetime.fromtimestamp(int(e['ts']) + self.time_shift)
					yield ({'id':id,'text':txt, 'start':start, 'end':None})

	def on_getCurrentEpg(self, cid):
		return self.on_getDayEpg(cid, syncTime())
	
	def on_getDayEpg(self, id, dt):
		params = {"cid": id, "day": dt.strftime("%d%m%y")}
		response = self.getJsonData(self.site+"/epg?", params, "EPG for channel %s" % id)
		return self.channel_day_epg(response)

	def getSettings(self):
		return self.settings
		
	def parseSettings(self, rawsett):
		self.settings['Language']           = {'id':'lang', 'value':rawsett['lang']['value'], 'vallist':rawsett['lang']['list']}
		self.settings['HTTP caching']       = {'id':'http_caching', 'value':rawsett['http_caching']['value'], 'vallist':rawsett['http_caching']['list']}
		self.settings['Bitrate']            = {'id':'bitrate', 'value':rawsett['bitrate']['value'], 'vallist':rawsett['bitrate']['list']}
		self.settings['Timeshift']          = {'id':'timeshift', 'value':int(rawsett['timeshift']['value']), 'vallist':range(0,24)}
		hlsSettingNotFound = (not 'hls_enabled' in rawsett)
		if hlsSettingNotFound:
			rawsett['hls_enabled']={'value':True}
		self.settings['HLS enabled'] = {'id':'hls_enabled', 'value':str(rawsett['hls_enabled']['value']),'vallist':['true','false']}
		if hlsSettingNotFound:
			self.pushSettings([(self.settings['HLS enabled'],'true')])

		self.settings['Cache size(seconds)']= {'id':'fwcaching', 'value':int(rawsett['fwcaching']['value']), 'vallist':rawsett['fwcaching']['list']}

		ar = [ (a['age'],a['descr']) for a in rawsett['ar']['list'] ]
		self.settings['Archive']={'id':'ar', 'value':rawsett['ar']['value'], 'vallist':ar}

		if isinstance(rawsett['stream_server']['list'], list):
			ss = [ (s['ip'],s['descr']) for s in rawsett['stream_server']['list'] ]
			self.settings['Stream server']={'id':'stream_server', 'value':rawsett['stream_server']['value'].encode('utf-8'), 'vallist':ss}

		self.settings['Pay code'] = {'id':'code', 'value':'000000000000', 'limits':-1}

	def pushSettings(self, sett):
		for s in sett:
			if s[0]['id'] == 'code':
				params = {s[0]['id']:s[1]}
				response = self.getJsonData(self.site+"/enter_pay_code?", params, "Push setting [%s] new value." % s[0]['id'])
			else:
				params = {'var':s[0]['id'],'val':s[1]}
				response = self.getJsonData(self.site+"/settings_set?", params, "Push setting [%s] new value." % s[0]['id'])
			code = int(response['message']['code'])
			if not (code in [2,3,4]): 
				raise APIException('Setting %s value %s not saved, reason: %s, code %d' % (s[0].name, s[1], response['message']['text'], code))
			if 'code' in s[0]['id']:
				self.getAccountData()
			else:
				s[0]['value'] = s[1]
